# Practica 5

> Node.js, Express.js

En la práctica 4 se migró el `TodoApp` al proyecto generado con [React Redux + Webpack Starter](https://github.com/ulises-jeremias/react-redux-webpack-starter). El mismo consume los recursos del `local storage` del navegador. Se deberá contar con el ejercicio resuelto para resolver esta práctica.

_Nota: Talvez se deberá modificar minimamente la implementación del lado del frontend dado que las tareas necesitarán una forma uníboca de ser referenciadas._

_Para consumir la API se puede utilizar [fetch](https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch) o [axios](https://github.com/axios/axios)._

Se deberán persistir los datos en alguna fuente de almacenamiento. La misma puede ser un archivo `.json` en el servidor.

En caso de no tenerlo resuelto se puede utilizar el [ejemplo](https://gitlab.com/lifiajs/curso-2019-1er-semestre/trabajo-final/tree/master/frontend/resources/examples/todo-app) de frontend subido al repositorio [Trabajo Final](https://gitlab.com/lifiajs/curso-2019-1er-semestre/trabajo-final).

## Ejercicio 1

Realiza una API utilizando express que suplante al local storage utilizado como servidor de recursos. Para esto se desea desarrollar la primer version de una API con la siguiente interfaz:

- **Listado de Tareas**

```python
GET /api/v1.0/tasks

# 200
{
    "message": "Listado de Tareas",
    "results": [Task],
}
```

- **Agregar tarea**

```python
POST /api/v1.0/tasks

# body
{
    "name": "Comer en carne",
    "description": "Soy una descripción",
    "date": "DD/MM/YYYY",
}

# 201
# devuelve recurso agregado
{
    "id": "i am a some kind of url friendly index",
    "name": "Comer en carne",
    "description": "Soy una descripción",
    "date": "DD/MM/YYYY",
    "done": false, # valor defecto
}
```

- **Actualizar tarea**

```python
PUT /api/v1.0/tasks/:id

# body
{
    "description": "Soy una descripción nueva",
    "done": true, # actualizo estado
}

# 200
# devuelve recurso actualizado
{
    "id": "i am a some kind of url friendly index",
    "name": "Comer en carne",
    "description": "Soy una descripción nueva",
    "date": "DD/MM/YYYY",
    "done": true,
}
```

- **Eliminar tareas**

```python
DELETE /api/v1.0/tasks

# 200
```

- **Eliminar tarea por id**

```python
DELETE /api/v1.0/tasks/:id

# 200
# devuelve recurso eliminado
{
    "id": "i am a some kind of url friendly index",
    "name": "Comer en carne",
    "description": "Soy una descripción",
    "date": "DD/MM/YYYY",
}
```

- **Not found**, **InternalServerError**, **Bad Request**

En caso de exitir un error se deberá retornar un obtejo de la siguiente forma:

```python
# <http status code>
{
    "status": <http status code>,
    "message": "Error message",
}
```

## Ejercicio 2 - Complementario

Implementar en NodeJs, sin utilizar Express, la API definida en el ejercicio anterior. Se deberán respetar todos los aspectos definidos anteriormente.
