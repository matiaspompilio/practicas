# Práctica 2

## Punto 1

Escribir una módulo con las siguientes funciones:

```js
/**
 * Pasa a mayúscula la primer letra del string
 * Ej: "people group".capitalize() === "People group"
 * Ej: capitalize("people group") === "People group"
 */
function capitalize() {}

/**
 * Convierte el string a snakecase
 * Los espacios se reemplazan por _
 * Ej: "person to yml".snakeize() === "person_to_yml"
 * Ej: snakeize("person to yml") === "person_to_yml"
 */
function snakeize() {}

/**
 * Convierte el string a camelcase
 * Los espacios se reemplazan por la siguiente letra en mayúscula
 * Ej: "person to yml".camelize() === "personToYml"
 * Ej: camelize("person to yml") === "personToYml"
 */
function camelize() {}

/**
 * Convierte el string a un formato de clase válido
 * Formato camelcase capitalizado
 * Ej: "people group".clasify() === "PeopleGroup"
 * Ej: clasify("people group") === "PeopleGroup"
 */
function classify() {}
```

Las funciones tienen que poder llamarse como métodos a strings y como funciones enviando el string como primer parámetro.

<div class="page"></div>

## Punto 2

Sobreescribir las siguiente funciones, utilizando sintaxis ES cuando convenga

### A)

```js
function squares(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).map(function(x) {
    return x * 2;
  });
}
```

### B)

```js
function getPeople(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).filter(function(elem) {
    return elem.name && elem.lastName;
  })
}
```

### C)

```js
function objectFromEntries(arr) {
  if (typeof(arr) === 'undefined') {
    return [];
  }

  return Array.from(arr || []).reduce(function(obj, entries) {
    obj[entries[0]] = entries[1];
    return obj;
  }, {});
}
```

<div class="page"></div>

### D)

```js
var tags = [ 'h1', 'h2', 'h3', 'h4', 'h5', 'p' ]

function toHtml(tag, str) {
  var validTag = false;
  for (i = 0; i < tags.length; i++) {
    if (tags[i] === tag) {
      validTag = true;
    }
  }

  if (!validTag) {
    return '';
  }

  if (str === undefined) {
    return '<' + tag + '></' + tag + '>';
  }

  return '<' + tag + '>' + str + '</' + tag + '>';
}
```

<div class="page"></div>

### Extra)

```js
function toYmlArray(array, level) {
  if (array === undefined || !Array.isArray(array)) {
    return '';
  }
  var tab = '';
  
  if (typeof(level) === 'undefined') {
    level = 0;
  } else {
    for (i = 0; i < level; i++) {
      tab += '  ';
    }
  }

  return array.reduce(function(str, elem) {
    return str += tab + '- ' + elem + '\n';
  }, '');
}

function toYml(data, level) {
  var yml = '';
  var tab = '';

  if (typeof(level) === 'undefined') {
    level = 0;
  } else {
    for (i = 0; i < level; i++) {
      tab += '  ';
    }
  }

  Object.keys(data).forEach(function(elem) {
    if (typeof(data[elem]) === 'object' && !Array.isArray(data[elem])) {
      yml += elem + ': \n';
      yml += toYml(data[elem], level+1);
    } else {
      if (Array.isArray(data[elem])) {
        yml += tab + elem + ':\n' + toYmlArray(data[elem], level+1);
      } else {
        yml += tab + elem + ': ' + data[elem];
      }
    }
    yml += '\n'
  })

  return yml;
}
```

<div class="page"></div>

## Punto 3

Escribir las funciones map y filter utilizando solamente la función reduce.

Las funciones tienen que poder llamarse como métodos a arreglos y como funciones enviando el arreglo como primer parámetro, y la función como segundo parámetro.

<div class="page"></div>

## Punto 4

Comparar las salidas de los siguientes códigos, y marque las diferencias.

```js
const ErrorFunction = function(msg, data) {
  this.message = msg;
  this.name = 'ErrorFunction';
  this.data = data;
}

try {
    throw new ErrorFunction('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}
```

```js
class ErrorClass {
  constructor(msg, data) {
    this.message = msg;
    this.name = 'ErrorClass';
    this.data = data;
  }
}

try {
    throw new ErrorClass('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}
```

```js
class ErrorClassExtends extends Error {
  constructor(msg, data) {
    super(msg);
    this.name = 'ErrorClassExtends';
    this.data = data;
  }
}

try {
    throw new ErrorClassExtends('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}
```