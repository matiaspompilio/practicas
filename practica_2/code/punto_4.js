const ErrorFunction = function(msg, data) {
  this.message = msg;
  this.name = 'ErrorFunction';
  this.data = data;
}

try {
    throw new ErrorFunction('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}

class ErrorClass {
  constructor(msg, data) {
    this.message = msg;
    this.name = 'ErrorClass';
    this.data = data;
  }
}

try {
    throw new ErrorClass('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}

class ErrorClassExtends extends Error {
  constructor(msg, data) {
    super(msg);
    this.name = 'ErrorClassExtends';
    this.data = data;
  }
}

try {
    throw new ErrorClassExtends('Error', {metadata: {msg: 'hola'}});
} catch(e) {
  console.log(e);
}