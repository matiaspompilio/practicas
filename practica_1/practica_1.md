# Práctica 1

## Punto 1

Dado el HTML `punto_1/index.html`, agregar la siguiente funcionalidad **SIN** modificar la estructura del archivo.

Se debe poder realizar las siguientes acciones sobre la lista de tareas:

- Crear una nueva tarea
- Dar por finalizada una tarea, moviendo la misma a una lista nueva de "tareas finalizadas"
- Limpiar todos los listados


_Tener en cuenta que se debe conservar el estado al reacargar la página_

## Punto 2

Utilizando solo la consola del navegador, se debe poder ocultar un elemento del DOM con solo hacer click sobre él